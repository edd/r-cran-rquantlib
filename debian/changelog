rquantlib (0.4.24-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 31 Jul 2024 11:00:53 -0500

rquantlib (0.4.23-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 26 Jul 2024 14:03:54 -0500

rquantlib (0.4.22-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 25 Apr 2024 06:51:19 -0500

rquantlib (0.4.21-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 01 Feb 2024 18:42:26 -0600

rquantlib (0.4.20-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 26 Nov 2023 15:16:35 -0600

rquantlib (0.4.19-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 07 Aug 2023 17:31:39 -0500

rquantlib (0.4.18-2) unstable; urgency=medium

  * Rebuilding for unstable following bookworm release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 25 Jun 2023 09:49:05 -0500

rquantlib (0.4.18-1) experimental; urgency=medium

  * New upstream release (into 'experimental' while Debian is frozen)

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 01 May 2023 20:19:27 -0500

rquantlib (0.4.17-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 25 Oct 2022 20:58:47 -0500

rquantlib (0.4.16-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 05 May 2022 19:37:20 -0500

rquantlib (0.4.15-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 20 Jan 2022 07:38:13 -0600

rquantlib (0.4.14-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 06 Oct 2021 19:05:59 -0500

rquantlib (0.4.13-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 02 Sep 2021 22:20:27 -0500

rquantlib (0.4.12-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 02 Apr 2020 08:23:12 -0500

rquantlib (0.4.11-2) unstable; urgency=medium

  * Source-only upload

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 19 Jan 2020 17:44:22 -0600

rquantlib (0.4.11-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 15 Jan 2020 08:54:21 -0600

rquantlib (0.4.10-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 07 Aug 2019 17:43:12 -0500

rquantlib (0.4.9-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 19 May 2019 08:13:10 -0500

rquantlib (0.4.8-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 17 Mar 2019 14:06:14 -0500

rquantlib (0.4.7-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 11 Dec 2018 05:37:09 -0600

rquantlib (0.4.6-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

  * Updated to be made compatible with Boost 1.67 or later

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 25 Nov 2018 10:59:10 -0600

rquantlib (0.4.5-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 10 Aug 2018 14:02:50 -0500

rquantlib (0.4.4-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/compat: Increase level to 9
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 08 Nov 2017 06:54:43 -0600

rquantlib (0.4.3-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 19 Aug 2016 14:58:51 -0500

rquantlib (0.4.2-3) unstable; urgency=medium

  * Folded in two startup-related changes from upstream
    - R/init.R: Condition check on quantlib-config on its presence
    - R/zzz.R: Correct version comparison
    
 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 12 Aug 2016 18:01:09 -0500

rquantlib (0.4.2-2) unstable; urgency=medium

  * Rebuilt against QuantLib 1.8		(Closes: #812286)

  * debian/control: Set Build-Depends: to current QuantLib version
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 04 Jul 2016 13:12:59 -0500

rquantlib (0.4.2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 03 Dec 2015 19:01:12 -0600

rquantlib (0.4.1-3) unstable; urgency=low

  * Rebuilt under QuantLib 1.7
  * debian/control: Updated Build-Depends: to libquantlib0-dev (>= 1.7)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 24 Nov 2015 20:24:34 -0600

rquantlib (0.4.1-2) unstable; urgency=low

  * debian/control: Add Build-Depends: on libquantlib0-dev (>= 1.6.2-2) to
    ensure Depends: on libquantlib0v5

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 24 Sep 2015 22:12:54 -0500

rquantlib (0.4.1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version, current
    QuantLib version and g++ (>= 4:5.2)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 11 Sep 2015 06:43:53 -0500

rquantlib (0.4.0-3) unstable; urgency=low

  * debian/rules: Set builttime spamp thanks to patch by Philipp Rinn
  							(Closes: #783058)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 21 Apr 2015 21:01:44 -0500

rquantlib (0.4.0-2) unstable; urgency=low

  * Rebuilt under QuantLib 1.5
  
  * debian/control: Set Build-Depends: to current QuantLib version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 11 Feb 2015 21:34:27 -0600

rquantlib (0.4.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Update Standards-Version: 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 02 Dec 2014 06:28:27 -0600

rquantlib (0.3.12-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 09 Mar 2014 12:24:43 -0500

rquantlib (0.3.11-3) unstable; urgency=low

  * Rebuilt under QuantLib 1.4
  * debian/control: Updated Build-Depends: to libquantlib0-dev (>= 1.4)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 01 Mar 2014 21:37:48 -0600

rquantlib (0.3.11-2) unstable; urgency=low

  * Rebuilt as required for Rcpp 0.11.0
  * debian/control: Added (Build-)Depends: on r-cran-rcpp (>= 0.11.0-1)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 03 Feb 2014 20:40:17 -0600

rquantlib (0.3.11-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 27 Jan 2014 20:26:18 -0600

rquantlib (0.3.10-3) unstable; urgency=low

  * (Re-)building with QuantLib 1.3 now in the Debian archive
  
  * debian/control: Set Build-Depends: to current R version 

  * debian/control: Set Build-Depends: to current QuantLib version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 02 Sep 2013 12:19:22 -0500

rquantlib (0.3.10-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 02 Apr 2013 06:04:22 -0500

rquantlib (0.3.10-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 18 Feb 2013 07:36:32 -0600

rquantlib (0.3.9-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R + Rcpp versions
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 02 Dec 2012 12:35:13 -0600

rquantlib (0.3.8-2) unstable; urgency=low

  * Rebuilding against QuantLib 1.2
  * debian/control: Update Build-Depends: to current QuantLib version
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 09 Apr 2012 12:59:11 -0500

rquantlib (0.3.8-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 12 Sep 2011 08:31:44 -0500

rquantlib (0.3.7-3) unstable; urgency=low

  * Debian-only build again new QL 1.1 pre-release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 07 May 2011 14:32:12 -0500

rquantlib (0.3.7-2) unstable; urgency=low

  * Debian-only released to address build regression
  
  * src/Makevars.in: Unconditionally add -fpermissive to permit build
    under g++-4.6 now used in Debian			(Closes: #625048)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 02 May 2011 20:32:19 -0500

rquantlib (0.3.7-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 04 Apr 2011 07:34:05 -0500

rquantlib (0.3.6-1) unstable; urgency=low

  * New upstream release
  * Added two casts to two files to fix FTBFS 		(Closes: #614435)

  * debian/control: Set (Build-)Depends: to current R version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 23 Feb 2011 11:43:31 -0600

rquantlib (0.3.5-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 30 Nov 2010 22:05:16 -0600

rquantlib (0.3.4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 09 Aug 2010 16:21:06 -0500

rquantlib (0.3.3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 04 Aug 2010 05:55:36 -0500

rquantlib (0.3.2-1) unstable; urgency=low

  * New upstream release build against QuantLib 1.0.0~20100112 (though it
    can also be built against current release 0.9.9)
  
  * debian/control: Set (Build-)Depends: on rcpp to at least 0.7.0, and 
    for RQuantLib to 1.0.0~20100112 (which is still in NEW)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 14 Jan 2010 22:28:06 -0600

rquantlib (0.3.1-1) unstable; urgency=low

  * New upstream release matching QuantLib 0.9.9

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 12 Dec 2009 08:32:26 -0600

rquantlib (0.3.0-1) unstable; urgency=low

  * New upstream release 
  
  * This version reflect the work of Khanh Nguyen that was part of the
    Google Summer of Code 2009 under the umbrella of the R Foundation with
    Dirk Eddelbuettel acting as mentor.  See the upstream changelog
    (installed as /usr/lib/R/site-library/RQuantLib/ChangeLog) for details

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 05 Sep 2009 21:07:25 -0500

rquantlib (0.2.11-2) unstable; urgency=low

  * Rebuilt under current the libquantlib0 which itself was rebuilt 
    for Boost 1.38					(Closes: #530771)
  * debian/control: Tightened Build-Depends: accordingly
  
  * debian/control: Changed Section: to new section 'gnu-r'
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.8.1

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 28 May 2009 19:12:07 -0500

rquantlib (0.2.11-1) unstable; urgency=low

  * New upstream release 0.2.11 for Rcpp 0.6.4

  * debian/control: Updated Build-Depends: on Rcpp to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Mar 2009 19:46:06 -0600

rquantlib (0.2.10-1) unstable; urgency=low

  * New upstream release 0.2.10 for QuantLib 0.9.7

  * debian/control: Added (Build-)Depends: for r-cran-rcpp now that
    the Rcpp sources have moved into their own package

  * debian/control: Updated Build-Depends: to current R version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 25 Dec 2008 20:21:17 -0600

rquantlib (0.2.9-1) unstable; urgency=low

  * New upstream release 0.2.9 for QuantLib 0.9.6	(Closes: #494240)

  * debian/control: Updated Build-Depends: accordingly to QL 0.9.6
  * debian/control: Updated Build-Depends: to current R version
  * debian/control: Updated Standard-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 09 Aug 2008 09:41:41 -0500

rquantlib (0.2.8-1) unstable; urgency=low

  * New upstream release 0.2.8 for QuantLib 0.9.0	(Closes: #456873)

  * debian/control: Updated Build-Depends: accordingly to QL 0.9.0
  * debian/control: Updated Build-Depends: to current R version
  * debian/control: Updated Standard-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 05 Jan 2008 05:56:20 -0600

rquantlib (0.2.7-1) unstable; urgency=low

  * New upstream release 0.2.7 
  * This version builds under QuantLib 0.8.1 		(Closes: #427200)

  * debian/control: Updated Build-Depends: updated to r-base-core (>= 2.5.1)
    libquantlib0-dev (>= 0.8.1), and libboost-(test-)dev (>= 1.34.0)
  * debian/control: Removed Build-Depends on gcc and g++ as these are 
    already implied by r-base-dev 
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Jul 2007 14:07:30 -0500

rquantlib (0.2.6-1) unstable; urgency=low

  * New upstream release 0.2.6

  * This release has been updated for Quantlib 0.4.0 and depends on it
  * debian/control: Build-Depends: updated to libquantlib0-dev (>= 0.4.0)
  * debian/control: Build-Depends: also updated to r-base-core (>= 2.4.1)
    and debhelper (>= 5.0.0)
  * debian/rules: Build with -g0 to reduce link time 
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 25 Feb 2007 21:02:58 -0600

rquantlib (0.2.5-1) unstable; urgency=low

  * Updated to release 0.2.5 which has been updated for QuantLib 0.3.13; 
    this required only some rather minor changes in the C++ sources

 -- Dirk Eddelbuettel <edd@debian.org>  Mon,  6 Nov 2006 21:55:51 -0600

rquantlib (0.2.4-1) unstable; urgency=low

  * Updated to release 0.2.4 which has been updated for QuantLib 0.3.13; 
    this required some changes in the fixed-income functions

  * configure.in: Now tests for QuantLib version 0.3.13 or later
  
  * tests/RQuantLib.R: Added the beginnings of unit-tests
  * tests/RQuantLib.Rout.save: Control output for unit tests

  * debian/control: Build-Depends: on the current unstable versions 
    'gcc (>= 4:4.1.1-6), g++ (>= 4:4.1.1-6), g++-4.1 (>= 4.1.1-10)' 
    which can build the package whereas the testing versions cannot.
  
 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 15 Aug 2006 22:19:15 -0500

rquantlib (0.2.3-1) unstable; urgency=low

  * Updated to version 0.2.3 
  
  * Source changes are complete adoption f RcppTemplate for all R/C++ 
    interfaces, and an upgrade to RcppTemplate version 4.2

  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Hence Build-Depends: updated to r-base-dev (>= 2.3.1)
  
  * debian/control: Standards-Version: increased to 3.7.2
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 23 Jul 2006 11:06:59 -0500

rquantlib (0.2.2-1) unstable; urgency=low

  * Updated to version 0.2.2 which will be uploaded to CRAN as well

  * This release contains more changes by Dominick Samperi, mostly concerning
    the R/C++ interface using his RCppTemplate package that is also on CRAN

  * This release also updates RQuantLib to the recent QuantLib release
    but should also be compatible for the previous QuantLib version
  
  * debian/control: Updated Build-Depends: to quantlib (>= 0.3.12)
  * debian/watch: Updated to version=3
  
 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 30 Mar 2006 20:53:21 -0600

rquantlib (0.2.1-1) unstable; urgency=low

  * Updated to version 0.2.1 which will be uploaded to CRAN as well

  * This release contains mostly changes by Dominick Samperi
    - several changes to the R/C++ interface (also in package Rcpp on CRAN)
    - small updates and corrections to a few other C++ file

  * demo/OptionsSurfaces.R: New file with an OpenGL demo. Due to hardware
    and driver issues with GL acceleration, this can crash on some machines,
    notably with ATI chipsets. 
  
  * debian/control: Updated Build-Depends: to r-base-dev (>= 2.2.0)
  * debian/watch: Updated regular expression
  
 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 10 Jan 2006 20:03:26 -0600

rquantlib (0.2.0-2) unstable; urgency=low

  * Rebuilt with new g++ for libstdc++ allocator transition
  * debian/control: Added Build-Depends g++-4.0 (>= 4.0.2-4) and
    updated the one for libquantlib0-dev to (>= 0.3.11-2) 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 18 Nov 2005 19:47:51 -0600

rquantlib (0.2.0-1) unstable; urgency=low

  * Updated to version 0.2.0 also uploaded to CRAN minutes ago

  * This release features significant contributions from Dominick Samperi:
    - BermudanSwaption      valuation using several short-rate models
    - DiscountCurve	    construct discount, forward and zero curves
    - Rcpp.{hpp,cpp}	    new C++ class designed to ease interfacing R
    - Windows support       maintains the Windows port (not in Debian)
    - configure 	    improved and more standardised configure.in 
  
  * Smaller changes by Dirk Eddelbuettel
    - src/zzz.R   	    improved library.dynam call (thanks, Brian Ripley)
    - configure.in	    check for QuantLib 0.3.11 or better
    - renamed sources	    files are now renamed to .cpp and .hpp
  
  * Debian changes
    - debian/control	    Build-Depends on boost 1.33.0-1, QuantLib 0.3.11
    - debian/post{inst,rm}  no longer build html at installation time
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 26 Oct 2005 21:58:26 -0500

rquantlib (0.1.13-1) unstable; urgency=low

  * Updated to version 0.1.13, also uploaded to CRAN minutes ago
  * Rebuilt against QuantLib 0.3.10-1 following the first gcc/g++ 4.0 release 

  * Implied volatilities are back as the mysterious segfaults are gone with the
    newer g++ versions 3.4.* and 4.0.*. 
  
  * src/*, R/*: code cleanup and decrufting, old commented out sections pruned
  
  * debian/control: Update Build-Depends: to gcc (>= 4:4.0), g++ (>= 4:4.0),
    libquantlib0-dev (>= 0.3.10), libboost-dev (>= 1.32.0+1.33.0-cvs20050727), 
    libboost-test-dev (>= 1.32.0+1.33.0-cvs20050727)
  * debian/control: Update Standards-Version: to 3.6.2.1
  * debian/post{inst,rm}: Call /usr/bin/R explicitly (thanks, Kurt Hornik)
  * debian/watch: Corrected regular expression (thanks, Rafael Laboissier)
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  6 Aug 2005 21:23:25 -0500

rquantlib (0.1.12-3) unstable; urgency=medium

  * Rebuilt again under QuantLib 0.3.9-3 with renamed libquantlib
  * debian/control: Update Build-Depends to libquantlib0-dev (>= 0.3.9-3)

  * Urgency set to medium to permit migration into testing

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 18 May 2005 23:42:12 -0500

rquantlib (0.1.12-2) unstable; urgency=medium

  * Rebuilt under QuantLib 0.3.9-2 with its improved shlibs file
  * debian/control: Update Build-Depends to libquantlib0-dev (>= 0.3.9-2)

  * Urgency set to medium to permit migration into testing
  
 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 May 2005 21:21:40 -0500

rquantlib (0.1.12-1) unstable; urgency=low

  * Updated to version 0.1.12, also uploaded to CRAN minutes ago
  * debian/control: Builds-Depends now R 2.1.0 and QL 0.3.8.rc.20050412

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 26 Apr 2005 22:09:13 -0500

rquantlib (0.1.11-1) unstable; urgency=low

  * Updated to version 0.1.11, also uploaded to CRAN minutes ago
  * debian/control: Updated Builds-Depends to R 2.0.1 and QuantLib 0.3.8

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 27 Dec 2004 22:05:38 -0600

rquantlib (0.1.10-2) unstable; urgency=low

  * Rebuilt under R 2.0.0
  * debian/control: Updated Build-Depends: and Depends: accordingly
  * debian/watch: Corrected, now points at RQuantLib

 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  9 Oct 2004 07:33:57 -0500

rquantlib (0.1.10-1) unstable; urgency=low

  * Updated to version 0.1.10, also uploaded to CRAN minutes ago
  * debian/control: Updated Builds-Depends and Depends to R 1.9.1
  * debian/control: Updated Standards-Version to 3.6.1.1
  * debian/post{inst,rm}: Call build-help.pl to update package.html list

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 12 Sep 2004 14:25:24 -0500

rquantlib (0.1.9-2) unstable; urgency=low

  * debian/control: Added libboost-dev, libboost-regex-dev, and 
    libboost-test-dev to Depends as needed for QuantLib 0.3.7

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 22 Jun 2004 08:39:31 -0500

rquantlib (0.1.9-1) unstable; urgency=low

  * Updated to version 0.1.9, also uploaded to CRAN minutes ago
  * man/{American,European,Barrier,Binary}Option.Rd: Fixes for problems
    suggested by Ajay Shah (Closes: #249240)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 26 May 2004 21:29:49 -0500

rquantlib (0.1.8-1) unstable; urgency=low

  * Updated to version 0.1.8, also uploaded to CRAN minutes ago
  * debian/rules: Yet another small update towards a common rules file
  * debian/control: Updated Build-Depends and Depends on newer R and QL

 -- Dirk Eddelbuettel <edd@debian.org>  Tue,  6 Apr 2004 20:33:56 -0500

rquantlib (0.1.7-1) unstable; urgency=low

  * Updated to version 0.1.7 uploaded to CRAN minutes ago
  * debian/rules: Another small update moving towards common cdbs file
  * debian/control: Updated Build-Depends and Depends on newer R and QL
  * debian/control: Increased Standards-Version to 3.6.1.0

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 25 Nov 2003 23:12:05 -0600

rquantlib (0.1.6-1) unstable; urgency=low

  * Updated to version 0.1.6 uploaded to CRAN minutes ago
  * debian/rules: Rewritten using (invariant) cdbs template
  * debian/control: Added cdbs to Build-Depends
  * debian/control: Updated Standards-Version to 3.6.0

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 31 Jul 2003 20:39:57 -0500

rquantlib (0.1.5-2) unstable; urgency=low

  * debian/control: Update Build-Depends (Closes: #195958)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue,  3 Jun 2003 21:45:18 -0500

rquantlib (0.1.5-1) unstable; urgency=low

  * Updated to version 0.1.5 uploaded to CRAN minutes ago
  * debian/control: Require r-base-core (>= 1.7.0) as new methods()
    package is now required.

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 31 May 2003 23:43:59 -0500

rquantlib (0.1.4-1) unstable; urgency=low

  * Initial Debian Release based on most recent CRAN release 0.1.4

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 11 May 2003 17:37:07 -0500


